const AppReducer = (state, action) => {
    switch (action.type) {
        case 'PLAY':
            return state;
        case 'STOP':
            return state;
        case 'RECORD':
            return state;
        case 'SAVE':
            return state;
    }
};

export default AppReducer;