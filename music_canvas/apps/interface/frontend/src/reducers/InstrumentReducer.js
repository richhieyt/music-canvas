const core = require('@magenta/music/node/core');

const InstrumentReducer = (state, action) => {
    console.log(state);
    console.log(action);
    switch (action.type) {
        case 'ADD_INSTRUMENT':
            return {
                instruments: state.instruments.concat(action.new_instrument)
            };

        case 'REMOVE_INSTRUMENT':
            console.log('INSIDE REDUCER!');
            console.log(state.instruments);
            state.instruments.filter((item) => {
                console.log(item);
                console.log(action.id);
                return item.id !== action.id;
            });
            return {
                instruments: state.instruments.filter(item => item.id !== action.id)
            };

        case 'CHANGE_INSTRUMENT':
            console.log('CHANGE_INSTRUMENT - Dispatched to reducer');
            let _instrs = state.instruments;
            console.log(_instrs);
            console.log(_instrs[action.info.instr_id]);
            _instrs[action.info.instr_id].info = action.info.instrument_info;
            return {
                instruments: _instrs
            };


        case 'CHANGE_CONTROLLER':
            console.log('CHANGE_CONTROLLER - Dispatched to reducer');
            let instrs = state.instruments;
            console.log(instrs);
            instrs[action.info.instr_id].controller = action.info.name;
            return {
                instruments: instrs
            };

        case 'CHANGE_C_MODEL':
            console.log('CHANGE_C_MODEL - Dispatched to reducer');
            let __instrs = state.instruments;
            console.log(__instrs);
            __instrs[action.info.instr_id].continuation_model = action.info.name;
            return {
                instruments: __instrs
            };


        case 'DELETE_ALL_INSTRUMENTS':
            console.log('DELETE_ALL_INSTRUMENTS - Dispatched to reducer');
            return {
                instruments: []
            };

        case 'PLAY_INSTRUMENT':
            console.log('PLAY_INSTRUMENT - Dispatched to reducer');
            let s_instrument = state.instruments[action.info.instr_id];
            let seq = s_instrument.recorder.getNoteSequence();
            console.log(seq);
            if (seq) {
                let quantized_seq = core.sequences.quantizeNoteSequence(seq, 4);
                let player = s_instrument.player;
                player.callbackObject = {
                    
                    run: (note) => {
                        let config = {
                            noteHeight: 6,
                            pixelsPerTimeStep: 30,  // like a note width
                            noteSpacing: 1,
                            noteRGB: '8, 41, 64',
                            activeNoteRGB: '240, 84, 119',
                        };
                        let visualizer = new core.PianoRollCanvasVisualizer(
                            quantized_seq,
                            document.getElementsByClassName('canvas0')[0],
                            config
                        );
                        visualizer.redraw(note)
                    },
                    stop: () => {console.log('done');}
                }
                player.loadSamples(quantized_seq).then(() => { player.start(quantized_seq) });
            }
 
            return state;

        case 'STOP_INSTRUMENT':
            console.log('STOP_INSTRUMENT - Dispatched to reducer');
            let _instrument = state.instruments[action.info.instr_id];
            _instrument.recorder.stop();
            console.log(_instrument)
            state.instruments[action.info.instr_id] = _instrument;
            return state;

        case 'RECORD_INSTRUMENT':
            console.log('RECORD_INSTRUMENT - Dispatched to reducer');
            let instrument = state.instruments[action.info.instr_id];
            instrument.recorder.initialize();
            instrument.recorder.start();
            state.instruments[action.info.instr_id] = instrument;
            return state;

        case 'SAVE_INSTRUMENT':
            console.log('SAVE_INSTRUMENT - Dispatched to reducer');
            return state;            

        case 'CONTINUE_PLAYING':
            console.log('CONTINUE_PLAYING - Dispatched to reducer');
            return state;

        case 'CHANGE_VOLUME':
            console.log('CHANGE_VOLUME - Dispatched to reducer');
            let v_instrs = state.instruments;
            console.log(__instrs);
            v_instrs[action.info.instr_id].volume = action.info.value;
            return {
                instruments: v_instrs
            };

        case 'CHANGE_SOUND_CONFIG':
            console.log('CHANGE_SOUND_CONFIG - Dispatched to reducer');
            return state;
    }
};

export default InstrumentReducer;