const PlayerReducer = (state, action) => {
    switch (action.type) {
        case 'PLAY_SELECTED_INSTRUMENTS':
            console.log('PLAY_SELECTED_INSTRUMENTS - inside Player Reducer');
            console.log(action.instruments);
            return state;
        case 'STOP_SELECTED_INSTRUMENTS':
            console.log('STOP_SELECTED_INSTRUMENTS - inside Player Reducer');
            return state;
        case 'RECORD_SELECTED_INSTRUMENTS':
            console.log('RECORD_SELECTED_INSTRUMENTS - inside Player Reducer');
            return state;
        case 'SAVE_SELECTED_INSTRUMENTS':
            console.log('SAVE_SELECTED_INSTRUMENTS - inside Player Reducer');
            return state;
        case 'LOOP_INSTRUMENTS':
            console.log('LOOP_SELECTED_INSTRUMENTS - inside Player Reducer');
            return state;
    }
};

export default PlayerReducer;