import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import VolumeUp from '@material-ui/icons/VolumeUp';
import Typography from '@material-ui/core/Typography';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import SaveIcon from '@material-ui/icons/Save';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CloseIcon from '@material-ui/icons/Close';
import WebMIDI from 'webmidi'  
import {InstrumentContext} from '../../store'

const useStyles = makeStyles(theme => ({
    button: {
        display: 'block',
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 170,
    },
    menuButton: {
        'background-color': '#e8e8e8',
        margin: '10px',
        float: 'center',
        padding: '10px',
    },
    continue: {
      'background-color': '#e8e8e8',
      margin: '10px',
    },
    settings: {
      float: 'right',
      margin: '10px',
      'background-color': '#e8e8e8'
    }
}));

function _available_instruments() {
    let instruments = [
        {
            name: 'Violin',
            value: 'violin'
        },    
        {
            name: 'Steinway Grand Piano',
            value: 'steinway-grand-piano'
        },
        {
            name: 'Granada Acoustic Guitar',
            value: 'granada-acoustic-guitar'
        },
    ];
    return instruments;
}

function _available_controllers() {
    navigator.requestMIDIAccess({sysex: true})
    let inputs = WebMIDI.inputs;
    let controllers = [];
    for (const input in inputs) {
        controllers.push({
            info: inputs[input]
        });
    }
    return controllers;
}

function _available_continuation_models() {
    let models = [
        {
            name: 'MelodyRNN',
            value: 'melodyrnn'
        },
        {
            name: 'Music Transformer',
            value: 'music_transformer'
        },
        {
            name: 'Custom Model',
            value: 'custom_model'
        }
    ];
    return models;
}

export default function InstrumentSelect(props) {
    const classes = useStyles();
    const [controller, setController] = React.useState('');
    const [instrument, setInstrument] = React.useState('');
    const [continuation_model, setContinuationModel] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [instruments, instr_dispatch] = React.useContext(InstrumentContext);

    const available_controllers = _available_controllers();
    const available_instruments = _available_instruments();
    const continuation_models = _available_continuation_models();
    const render_controllers = available_controllers.map((item, key) => {
        if (key.value == controller) {
            return <MenuItem value={item.info.name} active='true'>{item.info.name}</MenuItem>
        } else {
            return <MenuItem value={item.info.name}>{item.info.name}</MenuItem>
        }
    });
    const render_instruments = available_instruments.map((item, key) => {
        if (key.value == instrument) {
            return <MenuItem value={item.value} active='true'>{item.name}</MenuItem>
        } else {
            return <MenuItem value={item.value}>{item.name}</MenuItem>
        }
    });

    const render_c_models = continuation_models.map((item, key) => {
        if (key.value == continuation_model) {
            return <MenuItem value={item.value} active='true'>{item.name}</MenuItem>
        } else {
            return <MenuItem value={item.value}>{item.name}</MenuItem>
        }
    });

    const handleChangeController = (event, id) => {
        instr_dispatch({
            type: 'CHANGE_CONTROLLER',
            info: {
                name: event.target.value,
                instr_id: id
            },
        });
        setController(event.target.value);

    };

    const handleChangeInstrument = (event, id) => {
        console.log('Handler for changing the instrument settings!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'CHANGE_INSTRUMENT',
            info: {
                instr_id: id,
                instrument_info:{
                    name:event.target.value,
                } 
            },
        });
        setInstrument(event.target.value); 
    };

    const handleChangeCModel = (event, id) => {
        console.log('Handler for changing the instrument settings!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'CHANGE_C_MODEL',
            info:{
                name:event.target.value,
                instr_id: id,
            }
        });
        setContinuationModel(event.target.value);
    };

    const handleplayInstrument = (event, id) => {
        console.log('Handler for playing the recorded instrument track!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'PLAY_INSTRUMENT',
            info:{
                name: event.target.value,
                instr_id: id,
            }
        });
    };

    const handlestopInstrument = (event, id) => {
        console.log('Handler for stopping the recorded instrument track!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'STOP_INSTRUMENT',
            info:{
                name: event.target.value,
                instr_id: id,
            }
        });
    };

    const handlerecordInstrument = (event, id) => {
        console.log('Handler for recording instrument track!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'RECORD_INSTRUMENT',
            info:{
                name: event.target.value,
                instr_id: id,
            }
        });
    };

    const handlesaveInstrument = (event, id) => {
        console.log('Handler for saving the recorded instrument track!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'SAVE_INSTRUMENT',
            info:{
                name: event.target.value,
                instr_id: id,
            }
        });
        setContinuationModel(event.target.value);
    };

    const handleContinuePlayInstrument = (event, id) => {
        console.log('Handler for continuing the instrument track!');
        // Do some fetching from API using event.value
        console.log(id);
        console.log(props.instrument.id);
        instr_dispatch({
            type: 'CONTINUE_PLAYING',
            info:{
                name: event.target.value,
                instr_id: id,
            }
        });
        setContinuationModel(event.target.value);
    };


    const handleCloseThisInstrument = (id) => {
        console.log('Handler for closing instrument track!');
        instr_dispatch({
            type: 'REMOVE_INSTRUMENT',
            id: id,
        });
    };

    return (
        <Box>
            <FormControl className={classes.formControl}>
                <InputLabel id="controller-select-helper-label">MIDI Controller</InputLabel>
                    <Select
                        labelId="controller-select-helper-label"
                        id="controller-select-helper"
                        value={controller}
                        onChange={(event) => handleChangeController(event, props.instrument.id)}
                    >
                    {render_controllers}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="instr-select-helper-label">Instrument</InputLabel>
                        <Select
                            labelId="instr-select-helper-label"
                            id="instr-select-helper"
                            value={instrument}
                            onChange={(event) => handleChangeInstrument(event, props.instrument.id)}
                            placeholder="Select Instrument"
                        >
                            {render_instruments}
                        </Select>
                    </FormControl>
                    <IconButton onClick={(event) => handleplayInstrument(event, props.instrument.id)} edge="start" className={classes.menuButton} color="inherit">
                        <PlayArrowIcon/>
                    </IconButton>
                    <IconButton onClick={(event) => handlestopInstrument(event, props.instrument.id)} edge="start" className={classes.menuButton} color="inherit">
                        <StopIcon/>
                    </IconButton>
                    <IconButton onClick={(event) => handlerecordInstrument(event, props.instrument.id)} edge="start" className={classes.menuButton} color="inherit">
                        <FiberManualRecordIcon/>
                    </IconButton>
                    <IconButton onClick={(event) => handlesaveInstrument(event, props.instrument.id)} edge="start" className={classes.menuButton}   color="inherit">
                        <SaveIcon/>
                    </IconButton>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="cmodel-select-helper-label"> Continuation Model</InputLabel>
                        <Select
                            labelId="cmodel-select-helper-label"
                            id="cmodel-select-helper"
                            value={continuation_model}
                            onChange={(event) => handleChangeCModel(event, props.instrument.id)}
                            placeholder="Select Continuation Model"
                        >
                            {render_c_models}
                        </Select>
                    </FormControl>

                    <Button onClick={(event) => handleContinuePlayInstrument(event, props.instrument.id)} className={classes.continue}> Continue </Button>
                    <IconButton onClick={() => handleCloseThisInstrument(props.instrument.id)} edge="start" className={classes.settings} color="inherit">
                        <CloseIcon/>
                    </IconButton>
                    <IconButton edge="start" className={classes.settings}   color="inherit">
                        <SettingsIcon/>
                    </IconButton>

                </Box>
    );
}