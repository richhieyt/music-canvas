import React from 'react';
import InstrumentSelect from './InstrumentSelect'
import InstrumentSound from './InstrumentSound'
import Track from './Track'
import Icon from '@material-ui/core/Icon';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import VolumeUp from '@material-ui/icons/VolumeUp';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        'background-color': '#c9d4da',
        'margin-top': '3vh'
    },
    paper: {
        padding: theme.spacing(0),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    vol_container: {
        padding: '0px 20px 0px 16px'
    }
}));

// Canvas Component
//---------------------------------------------------------------------------------
function Instrument(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(30);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
    <div className={classes.root}>
        <Grid container spacing={3}>
            <Grid item lg={12}>
                <InstrumentSelect {...props}/>
            </Grid>
        </Grid>
        <Grid container spacing={3}>
            <Grid item lg={2}>
                <Grid container className={classes.vol_container}>
                    <Grid item lg={3}>
                        <VolumeUp />
                    </Grid>
                    <Grid item lg={9}>
                        <Slider value={value} onChange={handleChange} aria-labelledby="continuous-slider" />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item lg={10}>
                <canvas className={"canvas"+props.instrument.id}>Piano Roll Visualizer!</canvas>
            </Grid>
        </Grid>
    </div>
    );
}
export default Instrument;