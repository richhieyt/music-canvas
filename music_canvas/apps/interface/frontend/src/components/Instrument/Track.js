import React from 'react';
import ReactDOM from 'react-dom'
import {Button, ButtonGroup, ButtonToolbar, ToggleButtonGroup, ToggleButton} from 'react-bootstrap';

const core = require('@magenta/music/node/core');

const TWINKLE_TWINKLE = {
  notes: [
    {pitch: 60, startTime: 0.0, endTime: 0.5},
    {pitch: 60, startTime: 0.5, endTime: 1.0},
    {pitch: 67, startTime: 1.0, endTime: 1.5},
    {pitch: 67, startTime: 1.5, endTime: 2.0},
    {pitch: 69, startTime: 2.0, endTime: 2.5},
    {pitch: 69, startTime: 2.5, endTime: 3.0},
    {pitch: 67, startTime: 3.0, endTime: 4.0},
    {pitch: 65, startTime: 4.0, endTime: 4.5},
    {pitch: 65, startTime: 4.5, endTime: 5.0},
    {pitch: 64, startTime: 5.0, endTime: 5.5},
    {pitch: 64, startTime: 5.5, endTime: 6.0},
    {pitch: 62, startTime: 6.0, endTime: 6.5},
    {pitch: 62, startTime: 6.5, endTime: 7.0},
    {pitch: 60, startTime: 7.0, endTime: 8.0},  
  ],
  totalTime: 8
};

class Track extends React.Component {
    constructor(props) {
        super(props)
    }

    visualize_track(player) {
        console.log('Playing');
        this.props.player.resumeContext();
        let ns = this.props.recorder.getNoteSequence();
        let viz = new core.PianoRollCanvasVisualizer(ns, ReactDOM.findDOMNode(this.refs.canvas));
        viz.redraw();
        console.log('Done');
    }

	render() {
          	return(
            <div className="Track">
                <Button value='Viz Sequence' onClick={() => this.visualize_track()}>Viz Sequence</Button>
                <canvas ref='canvas' width='150' height='75'></canvas>
    		</div>

    	)
    }
}

export default Track;