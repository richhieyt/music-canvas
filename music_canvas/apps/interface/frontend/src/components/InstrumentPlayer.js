import React from 'react';
import Instrument from "./Instrument/Instrument"
import Icon from '@material-ui/core/Icon';
const core = require('@magenta/music/node/core');
import {InstrumentContext} from '../store';

const INITIAL_TEMPO = 70;

// InstrumentPlayer Component - This is the parent component for the interface
//---------------------------------------------------------------------------------
function InstrumentPlayer() {
    // REACT COMPONENT LIFECYCLE METHODS
    //---------------------------------------------------------------------------------
        const [state, dispatch] = React.useContext(InstrumentContext);
        let instruments = state['instruments'].map(function(instrument, index){
            return <Instrument instrument={instrument} dispatch={dispatch}/>;
        });
		return(
			<div className="InstrumentPlayer">
                <div className = "Instruments">
    				{instruments}
    			</div>
    		</div>
    	)
    }
    //---------------------------------------------------------------------------------


export default InstrumentPlayer;