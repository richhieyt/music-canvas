import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import StopIcon from '@material-ui/icons/Stop';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import MenuIcon from '@material-ui/icons/Menu';
import LoopIcon from '@material-ui/icons/Loop';
import Drawer from '@material-ui/core/Drawer';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {InstrumentContext} from '../store'
import {PlayerContext} from '../store'
import {AppContext} from '../store'
const core = require('@magenta/music/node/core');

// ---------------------------------------------------------------------------------
// Constants
// ---------------------------------------------------------------------------------
const drawerWidth = 240;
const SOUNDFONT_SALAMANDER_GP = 'https://storage.googleapis.com/magentadata/js/soundfonts/salamander';
const SOUNDFONT_SGM_PLUS = 'https://storage.googleapis.com/magentadata/js/soundfonts/sgm_plus';
// ---------------------------------------------------------------------------------
// CSS
// ---------------------------------------------------------------------------------
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        'background-color': '#5e7ec5',
        margin: '10px',
        float: 'center',
        padding: '10px',
    },
    title: {
        flexGrow: 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    statusUpdate: {
        
    }
}));
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
// HEADER Component - This is the parent component for the interface
// ---------------------------------------------------------------------------------
function Header() {
    
    // ---------------------------------------------------------------------------------
    // State information
    // ---------------------------------------------------------------------------------
    const classes = useStyles();
    const theme = useTheme();
    const [sidebar_open, setSidebarOpen] = React.useState(false);


    // ---------------------------------------------------------------------------------
    // Helper functions
    // ---------------------------------------------------------------------------------
    const handleDrawerOpen = () => {
        setSidebarOpen(true);
    };

    const handleDrawerClose = () => {
        setSidebarOpen(false);
    };

    const [app_context, app_dispatch] = React.useContext(AppContext);
    const [instruments, instr_dispatch] = React.useContext(InstrumentContext);
    const [player, player_dispatch] = React.useContext(PlayerContext);

    const handleAddInstrument = () => {
        console.log(instruments);
        console.log(instruments.instruments.length);
        let player = new core.SoundFontPlayer('https://storage.googleapis.com/magentadata/js/soundfonts/salamander');
        let recorder = new core.Recorder({
            callbackObject: {
                run(seq) {
                    if (seq) {
                        // tslint:disable-next-line:no-unused-expression
                        visualizer = mm.PianoRollCanvasVisualizer(
                            seq, document.getElementsByClassName('canvas0')[0]);
                        visualizer.redraw(seq[seq.length - 1], true);
                    }
                }
            }
        });
        instr_dispatch({
            type: 'ADD_INSTRUMENT',
            new_instrument: {
                id: instruments.instruments.length,
                controller: '',
                info: {},
                continuation_model: '',
                volume: 5,
                sound_config: {},
                recorder: recorder,
                player: player,
            }
        });
    };

    const handlePlaySelectedInstruments = () => {
        console.log('Handler for playing all selected instruments!');
        player_dispatch({
            type: 'PLAY_SELECTED_INSTRUMENTS',
            instruments: instruments,
        });
    };

    const handleStopSelectedInstruments = () => {
        console.log('Handler for stopping all selected instruments!');
        player_dispatch({
            type: 'STOP_SELECTED_INSTRUMENTS',
        });
    };

    const handleRecordAllInstruments = () => {
        console.log('Handler for recording all selected instruments!');
        player_dispatch({
            type: 'RECORD_SELECTED_INSTRUMENTS'
        });
    };

    const handleLoopAllInstruments = (value) => {
        console.log('Handler for looping playback of all selected instruments!');
        player_dispatch({
            type: 'LOOP_INSTRUMENTS',
            loop_value: value
        });
    };

    const handleDeleteAllInstruments = () => {
        console.log('Handler for deleting all selected instruments!');
        instr_dispatch({
            type: 'DELETE_ALL_INSTRUMENTS',
        });
    };

    // ---------------------------------------------------------------------------------
    // Component Lifecycle functions
    // ---------------------------------------------------------------------------------
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h4" className={classes.title}>
                        Music Maker
                    </Typography>
                    <IconButton onClick={() => handleAddInstrument()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <AddCircleIcon />
                    </IconButton>
                    <IconButton onClick={() => handlePlaySelectedInstruments()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        < PlayCircleFilledIcon/>
                    </IconButton>
                    <IconButton onClick={() => handleStopSelectedInstruments()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <StopIcon />
                    </IconButton>
                    <IconButton onClick={() => handleRecordAllInstruments()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <FiberManualRecordIcon />
                    </IconButton>
                    <IconButton onClick={() => handleLoopAllInstruments()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <LoopIcon />
                    </IconButton>
                    <IconButton onClick={() => handleDeleteAllInstruments()} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <DeleteForeverIcon />
                    </IconButton>
                    <IconButton onClick={handleDrawerOpen} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="right"
                open={sidebar_open}
                classes={{
                    paper: classes.drawerPaper,
                }}>
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronRightIcon />
                    </IconButton>
                </div>
            </Drawer>
        </div>
    );
}

export default Header;