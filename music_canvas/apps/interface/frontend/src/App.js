import React from 'react';

import Typography from '@material-ui/core/Typography';
import Canvas from './components/Canvas';
import Header from './components/Header';
import MIDIPiano from './components/MIDIPiano';
import {AppContext} from './store';
import {InstrumentContext} from './store';
import {PlayerContext} from './store';
import {AppReducer} from './reducers';
import {PlayerReducer} from './reducers';
import {InstrumentReducer} from './reducers';

// APP Component - This is the parent component for the interface
//---------------------------------------------------------------------------------
function App() {
    // REACT COMPONENT LIFECYCLE METHODS
    // ----------------------------------------------------------------------------- 
    const appInitial = {};
    const instrumentInitial = {
        instruments: []
    };
    const playInitial = {};
    return (
        <div id='MainApp'>
            <AppContext.Provider value={React.useReducer(AppReducer, appInitial)}>
                <InstrumentContext.Provider value={React.useReducer(InstrumentReducer, instrumentInitial)}>
                    <PlayerContext.Provider value={React.useReducer(PlayerReducer, playInitial)}>
                        <Header/>
                        <Canvas/>
                        <div id='piano'>
                            <MIDIPiano/>
                        </div>
                    </PlayerContext.Provider>
                </InstrumentContext.Provider>
            </AppContext.Provider>
        </div>
    );
    //---------------------------------------------------------------------------------
}

export default App;
