from rest_framework import serializers
from apps.backend.models import MIDITrack

class MIDITrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = MIDITrack
        fields = ('title', 'description')