from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from rest_framework import viewsets
from .serializers import MIDITrackSerializer
from apps.backend.models import MIDITrack

class MIDITrackView(viewsets.ModelViewSet):
    serializer_class = MIDITrackSerializer
    queryset = MIDITrack.objects.all()  