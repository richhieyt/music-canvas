from django.db import models

# Create your models here.
class MIDITrack(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField(default = 'Unknown MIDI track')

    def _str_(self):
        return self.title